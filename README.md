This repository contains my lesson's learned for the fantastic fast.ai MOOC.  I started this course in Sep of 2017 with Part 1 v1 (2017), but switched to Part 1 v2 (2018), when this new version was released.  In Feb of 2018, I applied to attend Part 2 v2 (2018) as international fellows and luckily got accepted.

Within each notebook(Lessons), you will find a brief description of the subject learned, explanation of the codes, and my lesson's learned. 

